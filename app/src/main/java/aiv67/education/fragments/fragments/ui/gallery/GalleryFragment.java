package aiv67.education.fragments.fragments.ui.gallery;

import aiv67.education.fragments.fragments.R;
import aiv67.education.fragments.fragments.databinding.FragmentGalleryBinding;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavBackStackEntry;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import org.jetbrains.annotations.NotNull;

import static aiv67.education.fragments.fragments.Utils.buildParams;
import static aiv67.education.fragments.fragments.Utils.parseParameters;

/**
 * Фрагмент галереи
 *
 * @author igor
 * @since 06.02.2022
 */
public class GalleryFragment extends Fragment {
    public static final String DIALOG_RESULT_KEY = "dialog_result";

    private GalleryViewModel galleryViewModel;
    private FragmentGalleryBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);

        binding = FragmentGalleryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textGallery;
        galleryViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        parseParameters(binding.parentFragmentTextView, binding.fromButtonTextView, getArguments());

        binding.button1.setOnClickListener(this::toHome);
        binding.button2.setOnClickListener(this::toHome);
        binding.button3.setOnClickListener(this::toSlideshow);
        binding.button4.setOnClickListener(this::toSlideshow);
        binding.button5.setOnClickListener(this::toDialogFragment);
        binding.button6.setOnClickListener(this::toDialogFragment);

        return root;
    }

    @Override
    public void onViewCreated(@NotNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavBackStackEntry currentBackStackEntry = NavHostFragment.findNavController(this).getCurrentBackStackEntry();
        if (currentBackStackEntry != null) {
            MutableLiveData<GalleryDialogFragment.GalleryDialogResult> liveData = currentBackStackEntry.getSavedStateHandle().getLiveData(DIALOG_RESULT_KEY);
            liveData.observe(getViewLifecycleOwner(), galleryDialogResult -> {
                binding.dialogResultTextView.setText(galleryDialogResult.toString());
            });
        }
    }

    private void toHome(View view) {
        toFragment(R.id.action_nav_gallery_to_nav_home, view);
    }

    private void toSlideshow(View view) {
        toFragment(R.id.action_nav_gallery_to_nav_slideshow, view);
    }

    private void toFragment(int fragmentId, View view) {
        Navigation.findNavController(view).navigate(fragmentId, buildParams(this, view));
    }

    private void toDialogFragment(View view) {
        String buttonName = ((Button) view).getText().toString();
        Navigation.findNavController(view).navigate(GalleryFragmentDirections.actionNavGalleryToGalleryDialogFragment(buttonName));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}