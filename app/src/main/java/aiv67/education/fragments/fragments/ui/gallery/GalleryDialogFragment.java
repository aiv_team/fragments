package aiv67.education.fragments.fragments.ui.gallery;

import aiv67.education.fragments.fragments.databinding.FragmentDialogGalleryBinding;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.lifecycle.SavedStateHandle;
import androidx.navigation.NavBackStackEntry;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

import static aiv67.education.fragments.fragments.ui.gallery.GalleryFragment.DIALOG_RESULT_KEY;

/**
 * Фрагмент диалога.
 *
 * @author Igor
 * @since 06.02.2022
 */
public class GalleryDialogFragment extends BottomSheetDialogFragment {
    FragmentDialogGalleryBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentDialogGalleryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NotNull View view,
                              Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GalleryDialogFragmentArgs fragmentArgs = GalleryDialogFragmentArgs.fromBundle(getArguments());
        binding.buttonName.setText(fragmentArgs.getButtonName());

        binding.buttonDone.setOnClickListener(this::done);
        binding.buttonCancel.setOnClickListener(this::cancel);
    }

    private void done(View view) {
        NavController navController = NavHostFragment.findNavController(this);

        NavBackStackEntry previousBackStackEntry = navController.getPreviousBackStackEntry();
        if (previousBackStackEntry != null) {
            GalleryDialogResult result = new GalleryDialogResult()
                    .setChecked(binding.checkBox.isChecked())
                    .setValue(binding.editTextValue.getText().toString());

            SavedStateHandle savedStateHandle = previousBackStackEntry.getSavedStateHandle();
            savedStateHandle.set(DIALOG_RESULT_KEY, result);
        }

        dismiss();
    }

    private void cancel(View view) {
        dismiss();
    }

    /**
     * Результат диалога
     */
    public static final class GalleryDialogResult implements Serializable {
        boolean isChecked;
        String value;

        public GalleryDialogResult() {
        }

        public GalleryDialogResult setChecked(boolean checked) {
            isChecked = checked;
            return this;
        }

        public GalleryDialogResult setValue(String value) {
            this.value = value;
            return this;
        }

        @NotNull
        @Override
        public String toString() {
            return "GalleryDialogResult{" +
                    "isChecked=" + isChecked +
                    ", value='" + value + '\'' +
                    '}';
        }
    }
}
