package aiv67.education.fragments.fragments.ui.slideshow;

import aiv67.education.fragments.fragments.R;
import aiv67.education.fragments.fragments.databinding.FragmentSlideshowBinding;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import static aiv67.education.fragments.fragments.Utils.buildParams;
import static aiv67.education.fragments.fragments.Utils.parseParameters;

/**
 * Фрагмент слайдшоу
 *
 * @author igor
 * @since 06.02.2022
 */
public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;
    private FragmentSlideshowBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                new ViewModelProvider(this).get(SlideshowViewModel.class);

        binding = FragmentSlideshowBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textSlideshow;
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        parseParameters(binding.parentFragmentTextView, binding.fromButtonTextView, getArguments());

        binding.button1.setOnClickListener(this::toHome);
        binding.button2.setOnClickListener(this::toHome);
        binding.button3.setOnClickListener(this::toGallery);
        binding.button4.setOnClickListener(this::toGallery);

        return root;
    }

    private void toHome(View view) {
        toFragment(R.id.action_nav_slideshow_to_nav_home, view);
    }

    private void toGallery(View view) {
        toFragment(R.id.action_nav_slideshow_to_nav_gallery, view);
    }

    private void toFragment(int fragmentId, View view) {
        Navigation.findNavController(view).navigate(fragmentId, buildParams(this, view));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}