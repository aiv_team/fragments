package aiv67.education.fragments.fragments.ui.home;

import aiv67.education.fragments.fragments.R;
import aiv67.education.fragments.fragments.databinding.FragmentHomeBinding;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import static aiv67.education.fragments.fragments.Utils.buildParams;
import static aiv67.education.fragments.fragments.Utils.parseParameters;

/**
 * Начальный фрагмент
 *
 * @author igor
 * @since 06.02.2022
 */
public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textHome;
        homeViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        parseParameters(binding.parentFragmentTextView, binding.fromButtonTextView, getArguments());

        binding.button1.setOnClickListener(this::toGallery);
        binding.button2.setOnClickListener(this::toGallery);
        binding.button3.setOnClickListener(this::toSlideshow);
        binding.button4.setOnClickListener(this::toSlideshow);

        return root;
    }

    private void toGallery(View view) {
        toFragment(R.id.action_nav_home_to_nav_gallery, view);
    }

    private void toSlideshow(View view) {
        toFragment(R.id.action_nav_home_to_nav_slideshow, view);
    }

    private void toFragment(int fragmentId, View view) {
        Navigation.findNavController(view).navigate(fragmentId, buildParams(this, view));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}