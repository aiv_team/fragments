package aiv67.education.fragments.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;

/**
 * Place purpose here.
 *
 * @author Igor
 * @since 06.02.2022
 */
public class Utils {
    /**
     * Собирает параметры
     *
     * @param parent родительский фрагмент
     * @param view   вызывающая кнопка, обязательно должна быть Button!!!
     * @return параметры
     */
    public static Bundle buildParams(Fragment parent, View view) {
        Bundle bundle = new Bundle();
        bundle.putString("parentFragment", parent.getClass().getSimpleName());
        bundle.putString("byButton", ((Button) view).getText().toString());

        return bundle;
    }

    /**
     * Разбор параметров
     *
     * @param parentFragmentTextView вьюха отображения родительского фрагмента
     * @param buttonTextView         вьюха отображения вызвавшей кнопки
     * @param arguments              параметры
     */
    public static void parseParameters(TextView parentFragmentTextView, TextView buttonTextView, Bundle arguments) {
        if (arguments != null) {
            parentFragmentTextView.setText(arguments.getString("parentFragment"));
            buttonTextView.setText(arguments.getString("byButton"));
        }
    }
}
